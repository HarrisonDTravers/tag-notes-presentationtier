# base image
FROM node:8

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
#RUN npm install react-scripts@3.0.1 --save

# Bundle app source
COPY . .

EXPOSE 3000

# start app
CMD ["npm", "run", "start"]