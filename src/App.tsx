import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
} from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import LanguageHandler, { LanguageEnum } from "./LanguageHandler";
import { SetSelector } from './routes/SetSelector';
import { Set } from './routes/set';
import { setSetName } from './actions/setActions';
import { SetState } from './reducers/setReducer';
import { StoreState } from './store/store';
import { getNotes } from './actions/noteActions';

/*class App extends React.Component<any, any>
{
  public constructor(props :any) 
  {    
    super(props);
    LanguageHandler.setLanguage(LanguageEnum.English); //Set default language
    this.state = 
    { 
      apiResponse: "", 
      title: LanguageHandler.get.Title,
      setName: "generatedSet" 
    };
    this.setSetName = this.setSetName.bind(this);
  }

  public setSetName(name : string)
  {
    this.setState({setName : name});
  }

  public render() : React.ReactNode 
  {
    return (      
    <div className="App">
      <Router>
        <header className="App-Header">
          <h1>{this.state.title}</h1>
        </header>
        <div className="App-Body">
          <Route path="/sets" render={props => <SetSelector {...props} onSetSelected={this.setSetName}/>}/>
          <Route path="/set" render={props => <Set {...props} setName={this.state.setName}/>}/>
        </div>
        <Redirect from="/" to="set"/>
      </Router>
    </div>)
  }
}*/

export default function App(props :any)
{
  const dispatch = useDispatch();
  LanguageHandler.setLanguage(LanguageEnum.English); //Set default language

  dispatch(setSetName("generatedSet"));  
  let setName = useSelector<StoreState, string>((state: StoreState) => state.set.setName);
  dispatch(getNotes(setName));

  return render(setName);

  function render(setName : string)
  {
    return (      
    <div className="App">
      <Router>
        <header className="App-Header">
          <h1>{LanguageHandler.get.Title}</h1>
        </header>
        <div className="App-Body">
          <Route path="/sets" render={props => <SetSelector {...props} onSetSelected={(setName : string)=>dispatch(setSetName(setName))}/>}/>
          <Route path="/set" render={props => <Set {...props} setName={setName}/>}/>
        </div>
        <Redirect from="/" to="set"/>
      </Router>
    </div>)
  }
}