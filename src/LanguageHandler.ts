import { LanguageStrings, EnglishStrings, JapaneseStrings } from './model/languages/LanguageStringsIndex';

export default class LanguageHandler
{
    private static language: LanguageStrings;

    public static setLanguage(language: LanguageEnum)
    {
        switch(language)
        {
            case LanguageEnum.Japanese:
            {                
                LanguageHandler.language = new JapaneseStrings();
                console.log("Nihernger");
                break;
            }
            default:
            {
                LanguageHandler.language = new EnglishStrings();
                console.log("Engrish");
            }
        }
    }

    public static get get() : LanguageStrings
    {
        return LanguageHandler.language;
    }
}

export enum LanguageEnum{English, Italian, Japanese}