import { NoteModel } from '../model/noteModel';
import { ThunkAction } from 'redux-thunk';
import { StoreState } from '../store/store';
import BusinessTierInterface from '../view/BusinessTierInterface';
import { TagModel } from '../model/tagModel';
import { IData, createData } from '../model/Data';
import { setIsLoading, SetAction } from './setActions';

export enum ActionType {ADD_NOTE="ADD_NOTE", REMOVE_NOTE="REMOVE_NOTE", EDIT_NOTE="EDIT_NOTE", SET_NOTES="SET_NOTES", DELETE_TAGONNOTE="DELETE_TAGONNOTE"}

export type NoteAction = { type: ActionType, payload: NoteModel } | 
                          { type: ActionType, payload: NoteModel[] } | 
                          { type: ActionType, payload: {note: NoteModel, tag: TagModel} }

  export const addNote = (note: NoteModel ): NoteAction => ({
    type: ActionType.ADD_NOTE,
    payload: note,
  });
  
  export const deleteNote = (note: NoteModel ): ThunkAction<unknown, StoreState, unknown, NoteAction> => async dispatch => {
    await BusinessTierInterface.deleteNote(note.id);
    dispatch({
      type: ActionType.REMOVE_NOTE,
      payload: note,
    });
  };

  export const deleteTagOnNote = ( tag: TagModel, note: NoteModel ): ThunkAction<unknown, StoreState, unknown, NoteAction> => async dispatch => {
    await BusinessTierInterface.deleteTagOnNote(note.id, tag.id);
    dispatch({
      type: ActionType.DELETE_TAGONNOTE,
      payload: {tag, note},
    });
  };
  
  export const editNote = (note: NoteModel): NoteAction => ({
    type: ActionType.EDIT_NOTE,
    payload: note,
  });

  export const setNotes = (notes: NoteModel[]): NoteAction => ({
    type: ActionType.SET_NOTES,
    payload: notes,
  });

  export const getNotes = (setName: string): ThunkAction<unknown, StoreState, unknown, NoteAction | SetAction> => async dispatch => {
    let newNotes : Array<NoteModel> = [];
    let notesJSON = await BusinessTierInterface.getNotes(setName);

    let noteObjects = JSON.parse(notesJSON);

    noteObjects.forEach((noteObj : any) =>
    {
      let tags : Array<TagModel> = [];
      console.log(noteObj);
      let data : IData = createData(noteObj.notedata, noteObj.datatype);
      let newNote = new NoteModel(noteObj.noteid, data, noteObj.creationdate, [], true, true, ()=>{}, (note : NoteModel) => {}/*dispatch(changeNote(noteObj))*/);
      newNote.deleteCallback = () => {dispatch(deleteNote(newNote))};

      for(let ii=0; ii<noteObj.tags.length; ii++)
      {
        let tagJson = noteObj.tags[ii];
        let newTag = new TagModel(tagJson.tagid, tagJson.tagname, tagJson.creationdate, tagJson.supertagid, ()=>{});
        newTag.deleteCallback = () => {dispatch(deleteTagOnNote(newTag, newNote))};
        tags.push(newTag);
      }

      newNote.tags = tags;

      newNotes.push(newNote);
    });

    dispatch(setNotes(newNotes));
    dispatch(setIsLoading(false));
  };