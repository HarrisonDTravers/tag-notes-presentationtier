export enum ActionType { SET_IS_LOADING="SET_IS_LOADING", SET_SET_NAME="SET_SET_NAME" }

export type SetAction = { type: ActionType, payload: Boolean} | { type: ActionType, payload: string}

export const setIsLoading = (isLoading : Boolean): SetAction => ({
    type: ActionType.SET_IS_LOADING,
    payload: isLoading
});

export const setSetName = (setName : string): SetAction => ({
    type: ActionType.SET_SET_NAME,
    payload: setName
});