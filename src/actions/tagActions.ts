import { ThunkAction } from 'redux-thunk';
import { TagModel } from '../model/tagModel'
import { StoreState } from '../store/store';
import BusinessTierInterface from '../view/BusinessTierInterface';

const enum ActionType {SET_TAGS="SET_TAGS"};

export type TagAction = { type: ActionType, payload: TagModel[] }

export const setTags = (tags: TagModel[]): TagAction => ({
    type: ActionType.SET_TAGS,
    payload: tags
});

export const getTags = (setName: string): ThunkAction<unknown, StoreState, unknown, TagAction> => async dispatch => {
    let tags: Array<TagModel> = await BusinessTierInterface.getTagsInSet(setName);
    dispatch(setTags(tags));
}