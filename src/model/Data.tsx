import React from 'react';
import "./Data.css";

export enum DataType {String = "string", Image = "image"};

export function createData(data : string, dataType : string)
{
    let dataInstance : IData;
    switch(dataType)
    {
        case "string" :
        {
            dataInstance = new StringData(data);
            break;
        }
        case "image" :
        {
            dataInstance = new ImageData(data);
            break;
        }
        default :
            throw new Error("Unsupported data type : " + dataType + ".");
    }
    return dataInstance;
}

export interface IData
{
    data : string;
    type : DataType;

    render() : JSX.Element;
}

export class StringData implements IData
{
    data : string;
    type : DataType;

    constructor(data : string)
    {
        this.data = data;
        this.type = DataType.String;
    }


    public render() : JSX.Element
    {
        return (<label className="string">{this.data}</label>);
    }
}

export class ImageData implements IData
{
    data : string;
    type : DataType;

    constructor(data : string)
    {
        this.data = data;
        this.type = DataType.Image;
    }

    public render() : JSX.Element
    {
        return (
            <img className="image"
                src={this.data}
                alt="new"
            />
        );
    }
}