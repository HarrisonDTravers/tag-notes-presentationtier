import LanguageStrings from "./LanguageStrings";

export default class EnglishStrings extends LanguageStrings
{
    public get Title() : string { return "#TagNotes"; }
    public get EnterSetName() : string { return "Enter set name."}
    public get LoadSet() : string { return "Load Set"}
    public get LoadingSet() : string { return "Retreiving Set..."}
    public get SetPrefix() : string { return "Set"; }
    public get TagsPrefix() : string { return "Tags"; }
    public get LoadingNotes() : string { return "Loading Notes"; }
    public get CreateNote_Click() : string { return "New Note"; }
    public get CreateNote_Type() : string { return "What type of note?"; }
    public get CreateNote_EnterText() : string { return "Enter the text for your new note."; }
    public get CreateNote_EnterTextPlaceholder() : string { return "My new note..."; }
    public get EmptySet_Text() : string { return "This set is empty, click New Note to get started!"}
    public get Create() : string { return "Create"; }
    public get Text() : string { return "Text"; }
    public get Or() : string { return "or"; }
    public get Image() : string { return "Image"; }
}