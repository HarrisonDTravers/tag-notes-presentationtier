import LanguageStrings from "./LanguageStrings";

export default class JapaneseStrings extends LanguageStrings
{
    public get Title() : string { return "#タグノート"; }
    public get EnterSetName() : string { return "せっとのなをおすください"}
    public get LoadSet() : string { return "せっとをろどする"}
    public get LoadingSet() : string { return "ローディングセット。。。"}
    public get SetPrefix() : string { return "セット"; }
    public get TagsPrefix() : string { return "タグ"; }
    public get LoadingNotes() : string { return "メモを読み込む"; }
    public get CreateNote_Click() : string { return "新しいメモを作成するには、ここをクリックしてください。"; }
    public get CreateNote_Type() : string { return "どんな種類のメモ？"; }
    public get CreateNote_EnterText() : string { return "新しいメモのテキストを入力してください。"; }
    public get CreateNote_EnterTextPlaceholder() : string { return "私の新しいメモ"; }
    public get EmptySet_Text() : string { return "このセットにはメモがありません！"}
    public get Create() : string { return "する"; }
    public get Text() : string { return "書いた"; }
    public get Or() : string { return "または"; }
    public get Image() : string { return "写真"; }
}