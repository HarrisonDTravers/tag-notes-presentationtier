export default abstract class LanguageStrings
{
    public abstract get Title() : string;
    public abstract get EnterSetName() : string;
    public abstract get LoadSet() : string;
    public abstract get LoadingSet() : string;
    public abstract get SetPrefix() : string;
    public abstract get TagsPrefix() : string;
    public abstract get LoadingNotes() : string;
    public abstract get CreateNote_Click() : string;
    public abstract get CreateNote_Type() : string;
    public abstract get CreateNote_EnterText() : string;    
    public abstract get CreateNote_EnterTextPlaceholder() : string;
    public abstract get EmptySet_Text() : string;
    public abstract get Create() : string;
    public abstract get Text() : string;
    public abstract get Or() : string;
    public abstract get Image() : string;
}