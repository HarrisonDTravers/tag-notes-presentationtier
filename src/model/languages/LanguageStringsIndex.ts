export { default as LanguageStrings } from "./LanguageStrings";
export { default as EnglishStrings } from "./EnglishStrings";
export { default as JapaneseStrings } from "./JapaneseStrings";