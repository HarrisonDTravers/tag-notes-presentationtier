import React from 'react';

import { TagModel } from './tagModel';
import { IData, StringData } from './Data';
import { NoteRenderer } from '../renderables/noteRenderer';

export class NoteModel
{
    public id : number;
    public data : IData;
    public creationDate : Date;
    public tags : Array<TagModel>;
    
    public canViewTags : Boolean;
    public canAddTags : Boolean;

    public deleteCallback : any;
    public changeCallback : any; //After this note is modified, ie. deleting a tag. The set needs to be notified that a note has changed, so that it can update put the new note into the notes list in the state and rerender the new note.

    constructor(id: number, data: IData, creationDate : Date, tags : Array<TagModel>, canViewTags : Boolean, canAddTags : Boolean, deleteCallback : any, changeCallback : any)
    {
        this.id = id;
        this.data = data;
        this.creationDate = creationDate;
        this.tags = tags;
        
        this.canViewTags = canViewTags;
        this.canAddTags = canAddTags;

        this.deleteCallback = deleteCallback;
        this.changeCallback = changeCallback;
    }
    
    public delete(): void
    {
        this.deleteCallback(this);
    }

    public GetRenderable() : JSX.Element
    {
        return <NoteRenderer key={this.id} model={this}/>;
    }    
    
    static createCustomTextNote(text: string)
    {
        return new NoteModel(-1, new StringData(text), new Date(), [], false ,false, null, null);
    }
}