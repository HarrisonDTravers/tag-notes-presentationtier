import React from 'react';

import { TagView } from '../renderables/tagView';

export class TagModel
{
    id : number;
    name : string;
    creationDate : Date;
    supertagid : Number;
    deleteCallback : any;

    constructor(id : number, name : string, creationDate : Date, supertagid : Number, deleteCallback : any)
    {
        this.id = id;
        this.name = name;
        this.creationDate = creationDate;
        this.supertagid = supertagid;
        this.deleteCallback = deleteCallback;
    }

    public static GetRenderables(tagModels : Array<TagModel>) : Array<JSX.Element>
    {
        var tagViews : Array<JSX.Element> = [];

        tagModels.forEach((model) => {tagViews.push(model.GetRenderable())});

        return tagViews;
    }

    public GetRenderable() : JSX.Element
    {
        return <TagView key={this.id} model={this} id={this.id} name={this.name} supertagid={this.supertagid} deleteCallback={this.deleteCallback}/>;
    }   
}