import React from 'react';

import { TagView } from '../renderables/tagView';
import { TagModel } from '../model/tagModel';
import { NoteModel } from '../model/noteModel';

export class TagOnNoteModel
{
    id : number;
    tag : TagModel;
    note : NoteModel;
    creationDate : Date;
    deleteCallback : any;

    constructor(id : number, tag : TagModel, note : NoteModel, creationDate : Date,  deleteCallback : any)
    {
        this.id = id;
        this.tag = tag;
        this.note = note;
        this.creationDate = creationDate;
        this.deleteCallback = deleteCallback;
    }

    public static GetRenderables(tagModels : Array<TagModel>) : Array<JSX.Element>
    {
        var tagViews : Array<JSX.Element> = [];

        tagModels.forEach((model) => {tagViews.push(model.GetRenderable())});

        return tagViews;
    }

    public GetRenderable() : JSX.Element
    {
        return <TagView key={this.id} model={this} id={this.id} name={this.tag.name} supertagid={this.tag.supertagid} deleteCallback={this.deleteCallback}/>;
    }   
}