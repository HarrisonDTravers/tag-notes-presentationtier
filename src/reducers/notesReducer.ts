import { NoteAction, ActionType, deleteNote } from "../actions/noteActions"
import { NoteModel as Note, NoteModel } from "../model/noteModel"
import { TagModel } from "../model/tagModel"

export interface NotesState {
    notes: Note[],    
    emptyNote: Note,
    loadingNote: Note
  }
  
  const initialState = {
    notes: [],
    emptyNote: Note.createCustomTextNote(""),
    loadingNote: Note.createCustomTextNote("")
  }
  
  export const notesReducer = (state:NotesState = initialState, action: NoteAction) => {
    switch(action.type){
      case ActionType.ADD_NOTE : {
        return {...state, notes: [action.payload, ...state.notes]}
      }
      case ActionType.REMOVE_NOTE : {
        return {...state, notes: state.notes.filter(x => x.id !== (<Note> action.payload).id)}
      }
      case ActionType.DELETE_TAGONNOTE : {
        var payload = (action.payload as {tag: TagModel, note: NoteModel})
        var targetNote = state.notes.find( x => x.id == payload.note.id )
        if(targetNote)
          targetNote.tags = targetNote.tags.filter(tag => tag.id !== payload.tag.id);
        return {...state, notes: state.notes};
      }
      case ActionType.EDIT_NOTE : {
        throw new Error("ActionType.EDIT_NOTE not yet supported");
      }
      case ActionType.SET_NOTES : {        
        return {...state, notes: action.payload}
      }
      default:
        return state
    }
  }