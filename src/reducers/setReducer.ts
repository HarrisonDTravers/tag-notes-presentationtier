import { SetAction, ActionType } from "../actions/setActions"

export interface SetState
{
    setName: string,
    isLoading: Boolean
}

const initialState = {
    setName: "",
    isLoading: true
}

export const setReducer = (state:SetState = initialState, action: SetAction) => {
    switch(action.type){
      case ActionType.SET_IS_LOADING : {
        return {...state, isLoading: action.payload}
      }
      case ActionType.SET_SET_NAME : {
        return {...state, setName: action.payload}
      }
        default:
          return state
      }
    }