import React from 'react';
import { Card, Button, Form } from 'react-bootstrap';

//src
import LanguageHandler from "../LanguageHandler";
import BusinessTierInterface from "../view/BusinessTierInterface"
import * as Data from "../model/Data";

//css
import TextIcon from '../images/Text_Icon.png';
import ImageIcon from '../images/Image_Icon.png';
import './NewNoteCard.css'
import { NoteModel } from '../model/noteModel';

export class NewNoteCard extends React.Component<any, any>
{
    constructor(props: any)
    {
        super(props);
        this.state = {
            render: this.getNewNoteCard(),
            nodeData: Data.DataType.String
        };
        this.displayNewNoteTypeChoice = this.displayNewNoteTypeChoice.bind(this);
        this.displayNewStringNote = this.displayNewStringNote.bind(this);
        this.createNewNote = this.createNewNote.bind(this);
    }

    displayNewStringNote()
    {
        this.setState({
            dataType: Data.DataType.String,
            render:(
            <Card className="StringNote-Card">
                <h1 className="StringNote-h1">{LanguageHandler.get.CreateNote_EnterText}</h1>                
                <Form.Group controlId="formNoteText">
                    <Form.Control bsPrefix="TextArea" as="textarea" onChange={this.onNoteDataChange.bind(this)} rows={5} placeholder={LanguageHandler.get.CreateNote_EnterTextPlaceholder}/>
                </Form.Group>
                <Button onClick={this.createNewNote.bind(this)}>{LanguageHandler.get.Create}</Button>
            </Card>
            )
        });
    }

    async createNewNote()
    {
        let noteData : Data.IData;
        switch(this.state.dataType)
        {
            case Data.DataType.String:
            {
                noteData = new Data.StringData(this.state.noteData);
                break;
            } 
            case Data.DataType.Image:
            {
                noteData = new Data.ImageData(this.state.noteData);
                break;
            }
            default:
            {
                throw new Error("DataType " + this.state.dataType + " is not valid");
            }
        }
        let resJSON = await BusinessTierInterface.putNoteData(noteData.data, noteData.type, this.props.setName);
        let regex = /\{\"row\":\"\(|\,\\\"|\\\"\)\"\}/
        let resArr: Array<string> = resJSON.split(regex);
        let newNote : NoteModel =  new NoteModel(Number.parseInt(resArr[1]), noteData, new Date(Date.parse(resArr[2])), [], true, true, null, null);
        this.props.onNewNote(newNote);
        this.setState({render: this.getNewNoteCard()});
    }

    displayNewNoteTypeChoice() : void
    {
        this.setState({render: 
            <Card className="Card">
                <h1>{LanguageHandler.get.CreateNote_Type}</h1>
                <Button className="TextOrImage-Btn" onClick={this.displayNewStringNote.bind(this)}>
                    <img className="TextOrImage-Img" src={TextIcon}/>
                    <h2 className="TextOrImage-Text">{LanguageHandler.get.Text}</h2>
                </Button> 
                <label className="Medium-Label">{LanguageHandler.get.Or}</label>
                <Button className="TextOrImage-Btn">
                    <img className="TextOrImage-Img" src={ImageIcon}/>
                    <h2 className="TextOrImage-Text">{LanguageHandler.get.Image}</h2>
                </Button>
            </Card>
        })
    }

    getNewNoteCard() : JSX.Element
    {
        return (
            <a onClick={this.displayNewNoteTypeChoice.bind(this)}>
                <Card className="Card"> 
                    {LanguageHandler.get.CreateNote_Click}
                </Card>
            </a>
        );
    }

    onNoteDataChange(event: any): void
    {
        this.setState({noteData: event.currentTarget.value}) ;
    }

    render() : JSX.Element
    {
        return this.state.render;
    }
}