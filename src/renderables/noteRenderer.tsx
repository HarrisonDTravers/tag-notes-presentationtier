import React from 'react';
import {    
    Button,
    Card,
    OverlayTrigger
   } from 'react-bootstrap';
import { NoteModel } from '../model/noteModel';

//src
import LanguageHandler from "../LanguageHandler";
import { TagModel } from '../model/tagModel';

//css
import DeleteIcon from '../images/Delete_Icon.png';

const popoverAddTag = <h1>popoverAddTag</h1>

    export function NoteRenderer(props: {model: NoteModel}) : JSX.Element
    {
        let deleteButton;
        let cardBody

        if(props.model.deleteCallback != null) //If the deleteCallback exists, show the delete button
        {
            deleteButton = (
            <Button onClick={props.model.deleteCallback}>
                <img className="Img-SmallIcon" alt="delete" src={DeleteIcon}/>
            </Button>
            )
        }

        if(props.model.canViewTags)
        {
            let tagsLabel;
            let addTagButton;

            tagsLabel = LanguageHandler.get.TagsPrefix + " : ";

            if(props.model.canAddTags)
            {
                addTagButton = (
                    <OverlayTrigger trigger="click" placement="right" overlay={popoverAddTag}>
                        <Button className="Tag-Add">+</Button>
                    </OverlayTrigger>
                    )
            }

            cardBody = (                
                <Card.Body>
                    {tagsLabel} 
                    {TagModel.GetRenderables(props.model.tags)}
                    {addTagButton}
                </Card.Body>   
            )
        }

        let card = (        
            <Card>
                <Card.Title>
                {deleteButton}
                <div>
                    {props.model.data.render()}
                </div>
                </Card.Title>   
                {cardBody}  
            </Card>);


        return card
    }