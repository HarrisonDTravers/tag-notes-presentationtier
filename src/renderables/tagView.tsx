import React from 'react';
import {    
    Button,
    Card
   } from 'react-bootstrap';

import { TagModel } from '../model/tagModel';
import { IData, StringData } from '../model/Data';
import { NoteModel as NoteModel } from '../model/noteModel';

//src
import LanguageHandler from "../LanguageHandler";

//css
import DeleteIcon from '../images/Delete_Icon.png';

export class TagView extends React.Component<any, any>
{

    constructor(props : any)
    {
        super(props);

        this.state=(
            {
                model: props.model
            }
        )
    }

    public render() : JSX.Element
    {        
        let deleteButton : any;

        if(this.state.model.deleteCallback != null)
        {
            deleteButton = (
                <Button onClick={this.state.model.deleteCallback}>
                    <img className="Img-SmallIcon" alt="delete" src={DeleteIcon}/>
                </Button>
            )
        }

        return (
        <a className="Tag">
            {this.state.model.name}
            {deleteButton}
        </a>
        )
    }
}