import React from 'react';

import {
  Redirect,
} from "react-router-dom";

//From src
import BusinessTierInterface from '../view/BusinessTierInterface';

import Button from "react-bootstrap/Button";

//css
import './SetSelector.css';
import LanguageHandler from '../LanguageHandler';

export class SetSelector extends React.Component<any, any>
{
    constructor(props : any)
    {
        super(props);
        this.state = {
            redirect: "",
            infoMessage: LanguageHandler.get.EnterSetName,
            setName: "",
            getResponse: ""
        }
        this.loadSet = this.loadSet.bind(this);
        this.updateSetNameInState = this.updateSetNameInState.bind(this);
    }

    public async loadSet()
    {
        this.setState({infoMessage: LanguageHandler.get.LoadingSet})
        try
        {
            if(await BusinessTierInterface.getSetExists(this.state.setName))
            {    
                this.props.onSetSelected(this.state.setName);
                this.setState({ redirect: "/notes" })   
            }
            else
            {            
                this.setState({ getResponse: "", infoMessage: "Set does not exist!" })  
            }
        }
        catch(err)
        {
            console.log(err);
            this.setState({ getResponse: "", infoMessage: "Unable to contact server, please try again later." })  
        }
    }

    public updateSetNameInState(event : any)
    {
        this.setState({setName : event.target.value})
    }

    public render()
    {
        let renderReturn;
        if(this.state.redirect !== "")
        {
            renderReturn = <Redirect to={this.state.redirect}/>
        }
        else
        {
            renderReturn = (
                <div>
                    <div>
                        <input onChange={this.updateSetNameInState}></input>
                        <Button onClick={this.loadSet}>{LanguageHandler.get.LoadSet}</Button>                        
                    </div>
                    <div>
                        <label>{this.state.infoMessage}</label>
                    </div>
                        <div><label>{this.state.getResponse}</label>
                    </div>
                </div>
            );
        }

        return renderReturn;
    }
}