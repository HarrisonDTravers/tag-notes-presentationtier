import React, { useEffect } from 'react';
import CardColumns from 'react-bootstrap/CardColumns';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import {
  Redirect
} from "react-router-dom";


//From src
import BusinessTierInterface from '../view/BusinessTierInterface';
import { NoteModel } from "../model/noteModel";
import { TagModel } from "../model/tagModel";
import { IData, createData } from "../model/Data";
import { NewNoteCard } from "../renderables/NewNoteCard";
import LanguageHandler from "../LanguageHandler";

//css
import './set.css'
import { addNote, deleteNote, getNotes } from '../actions/noteActions';
import { useDispatch, useSelector } from 'react-redux';
import { NotesState } from '../reducers/notesReducer';
import { SetState } from '../reducers/setReducer';
import { StoreState } from '../store/store';
import { setIsLoading } from '../actions/setActions';

export function Set(props: {setName: string})
{
  const dispatch = useDispatch();
  const selector = useSelector;

  let notes : Array<NoteModel>;
  let tags : Array<TagModel>;
  
  useEffect(() => 
  {
    getTagsInSet(props.setName);
  }, []);

  return render();

  function getTagsInSet(setName: string)
  {
    BusinessTierInterface.getTagsInSet(setName).then((tags: Array<TagModel>) => {
      /*dispatch(addTagsInSet(tags));*/
    })
  }

  async function newNote(note: NoteModel) : Promise<void>
  {
    note.deleteCallback = ()=>dispatch(deleteNote(note));
    dispatch(addNote(note));
  }

  function render()
  {
    let noteRenders: Array<JSX.Element> = [];
    let cardColumn;
    let setState: SetState;
    let notesState : NotesState;

    setState = selector<StoreState, SetState>(
      (state) => state.set
    )
    
    notesState = selector<StoreState, NotesState>(
      (state) => state.notes
    );
    
    notesState.notes.forEach((note: NoteModel) => {
      noteRenders.push(note.GetRenderable());
    })
    
      
    cardColumn = (
      <CardColumns className="CardColumns">
        <NewNoteCard setName={setState.setName} onNewNote={newNote}/>
        {notesState.notes.length<=0?NoteModel.createCustomTextNote(LanguageHandler.get.EmptySet_Text).GetRenderable():[]}
        {noteRenders}
      </CardColumns>);

    if(setState.isLoading)
    {
      return NoteModel.createCustomTextNote(LanguageHandler.get.LoadingNotes).GetRenderable();
    }
    else
    {

      return (          
        <div>
          <h1>{LanguageHandler.get.SetPrefix} {setState.isLoading}</h1>
          {cardColumn}
        </div>    
      );
    }    
  }
}

