import { createStore, combineReducers, applyMiddleware } from 'redux';
import {notesReducer, NotesState} from '../reducers/notesReducer'
import {setReducer, SetState} from '../reducers/setReducer';
import thunk from 'redux-thunk';

export interface StoreState{
    notes: NotesState,
    set: SetState
}

export const rootReducer = combineReducers({notes: notesReducer, set: setReducer})

export type AppState = ReturnType<typeof rootReducer>

export const store = createStore(rootReducer, applyMiddleware(thunk));