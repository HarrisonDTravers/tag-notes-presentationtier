import { TagModel } from "../model/tagModel";

export default class BusinessTierInterface
{
    public static async getSetExists(setName : string) : Promise<boolean>
    {
        let exists : boolean;
        exists = false;
        
        try
        {
            let res : any; //response type

            res = await fetch('http://localhost:3002/sets', {
                    method: 'get',
                    headers: new Headers({'setName' : setName})
                });
            
            if(res.status === 200)
            {
                exists = true;
            }
            else if(res.status === 400)
            {
                exists = false;
            }
        } catch (err) { throw new Error(err); }

        return exists;
    }

    public static async getNotes(setName : string) : Promise<string>
    {
        let msg = "";
        try
        {
            let res : any; //response type
            res = await fetch('http://localhost:3002/notes', {
                    method: 'get',
                    headers: new Headers({'setName' : setName})
                });
            
            if(res.status === 200)
            {
                msg = await res.text();
            }
            else if(res.status === 400)
            {
                throw new Error (await res.text());
            }            
        } catch (err) { throw new Error(err); }
        
        return msg;
    }

    public static async putNoteData(noteData: string, dataType: string, setName: string) : Promise<string>
    {
        let msg = "";
        try
        {
            let res : any; //response type

            res = await fetch('http://localhost:3002/notes', {
                    method: 'put',
                    headers: new Headers({
                        'noteData' : noteData,
                        'dataType' : dataType,
                        'setName' : setName
                    })
                });
            
            if(res.status === 200)
            {
                msg = await res.text();
            }
            else if(res.status === 400)
            {
                throw new Error ('Set doesn\'t exist');
            }
        } catch (err) { throw new Error(err); }

        return msg;
    }

    public static async deleteNote(noteId: Number) : Promise<void>
    {
        try
        {
            let res : any; //response type

            res = await fetch('http://localhost:3002/notes', {
                    method: 'delete',
                    headers: new Headers({
                        'noteId' : noteId.toString()
                    })
                });

            if(res.status === 400)
            {
                throw new Error ('Target note doesn\'t exist');
            }
            else if(res.status === 500)
            {
                throw new Error ('Server error');
            }
        } 
        catch (err) 
        { 
            throw new Error(err); 
        }
    }

    public static async deleteTagOnNote(noteId: number, tagId: number) : Promise<void>
    {
        try
        {
            let res : any; //response type

            res = await fetch('http://localhost:3002/tags/tagsonnotes', {
                    method: 'delete',
                    headers: new Headers({
                        'noteId' : noteId.toString(),
                        'tagId' : tagId.toString()
                    })
                });

            if(res.status === 400)
            {
                throw new Error ('Target tagonnote doesn\'t exist');
            }
            else if(res.status === 500)
            {
                throw new Error ('Server error');
            }
        } 
        catch (err) 
        { 
            throw new Error(err); 
        }
    }
    
    public static async getTagsInSet(setName: string) : Promise<Array<TagModel>>
    {
        let tagModels : Array<TagModel> = [];

        try
        {
            let res : any; //response type
            let tagObjects;

            res = await fetch('http://localhost:3002/tags/tagsinset', {
                    method: 'get',
                    headers: new Headers({
                        'setName' : setName.toString(),
                    })
                });

            if(res.status === 500)
            {
                throw new Error ('Server error');
            }

            tagObjects = JSON.parse(await res.text());
            await tagObjects.forEach((tagObj : any) =>
            {
                tagModels.push(new TagModel(tagObj.tagid, tagObj.tagname, tagObj.creationdate, tagObj.supertagid, ()=>{}));
            });
        } 
        catch (err) 
        { 
            throw new Error(err); 
        }

        return tagModels;
    }
}